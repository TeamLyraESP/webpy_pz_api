import web

from app.db import init_data

urls = (
    '/pos/(.*)/charge', 'app.controller.charges_controller.ChargeCreation',

    '/charges/(.*)', 'app.controller.charges_controller.ChargeHandle',

    '/redirect/(.*)', 'app.controller.charges_controller.ChargeRedirect',

    '/test', 'testing.controller.main.Test'
)

if __name__ == "__main__":
    init_data.init()
    app = web.application(urls, globals())
    app.run()