import json
import time
import web
import app.model.charge

from pzlib import pzlogic
from app.db import charges_operations
from config.apilogging import log



def create_charge(shop_id, data):
    """Takes the data to creates the charge and calls Lyra API to create one Payment.
    After that it takes all the information and create a JSON to return
    """

    #Creates the payment using the form V2
    cache_id = pzlogic.create_lyra_payment(shop_id, data['currency'], data['amount'])

    #Ask Lyra's service to retrieve a json with the the paymentcontext information
    payment_json = pzlogic.get_payment_creation_info(cache_id)

    charge = app.model.charge.get_charge_obj()
    methods = []
    links = []

    try:

        payment = json.loads(payment_json)

        charge['created_at'] = time.strftime("%Y-%m-%d %H:%M:%S")
        charge['updated_at'] = time.strftime("%Y-%m-%d %H:%M:%S")
        charge['amount'] = (payment['amount']['cents'])/100.0
        charge['currency'] = payment['amount']['currency']
        charge['status'] = "incomplete"
        charge['messages'] = "PAYMENT_INSTRUMENT_REQUIRED"
        charge['cache_id'] = cache_id
        charge['trans_id'] = payment['transaction_id']
        charge['site_id'] = shop_id
        charge['mode'] = str(payment['context']['mode']).upper()
        charge['used_instruments'] = ""

        for method in payment['methods']:
            methods.append({'method': method['card_type']})
        charge['available_methods'] = methods

        # save the charge in the DB
        charges_operations.add_charge(charge)

        # get the charge id
        charge_id = charges_operations.get_charge_id(cache_id)
        charge['id'] = charge_id

        origin_url = web.ctx.homedomain
        links.append(
            {'href': origin_url + '/redirect/' + str(charge_id),
            'rel': 'redirect',
            'method': 'get'}
        )
        links.append(
            {'href': origin_url + '/charges/' + str(charge_id),
            'rel': 'self',
            'method': 'get'}
        )
        links.append(
            {'href': origin_url + '/charges/' + str(charge_id),
            'rel': 'update',
            'method': 'put'}
        )
        charge['links'] = links



    except Exception as e:
        log.error("JSON format error. Message: " + e.message)

    return json.dumps(charge)

def get_payment_page(cache_id):
    return pzlogic.get_payment_page(cache_id)

def delete_charge(charge_id):
    success = charges_operations.delete_charge(charge_id)
    return success

def get_payment_info(charge_id):
    charge_bd = charges_operations.get_charge(charge_id)[0]
    payment_info =  pzlogic.get_payment_info(charge_bd)

    #TODO: parse response
    charge = app.model.charge.get_charge_obj()
    if payment_info.errorCode == 0:
        charge['status'] = "complete"
        charge['messages'] = ""
        charge['available_methods'] = []
        charge['id'] = charge_id
        charge['updated_at'] = time.strftime("%Y-%m-%d %H:%M:%S")
        charge['used_instruments'] = [
            {
                "method": payment_info.cardInfo.cardBrand,
                "card_number": payment_info.cardInfo.cardNumber,
                "exp_month": payment_info.cardInfo.expiryMonth,
                "exp_year": payment_info.cardInfo.expiryYear
            }
        ]
        charge['transactions'] = [
            {
                "status": "to_capture",
                "amount": payment_info.paymentGeneralInfo.amount/100.0,
                "currency": payment_info.paymentGeneralInfo.currency,
                "three_d_secure": {},
                "authorization": {}
            }
        ]
        origin_url = web.ctx.homedomain
        charge['links'] = [
            {
                'href': origin_url + '/charges/' + str(charge_id),
                'rel': 'self',
                'method': 'get'
            }
        ]

        charges_operations.update_charge(charge)
    else:
        log.error("Error")

    return json.dumps(charge)


