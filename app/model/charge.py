
def get_charge_obj():
    return {
        "amount" : 0.0,
        "available_methods" : {},
        "created_at" : None,
        "currency" : None,
        "status"  : None,
        "id" :  None,
        "updated_at" : None,
        "messages" : None,
        "used_instruments" : {},
        "links" : {},
        "cache_id" : None,
        "trans_id" : None,
        "site_id" : None,
        "mode": None,
        "transactions": []
    }
