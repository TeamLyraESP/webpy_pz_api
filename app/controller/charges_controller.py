import json
import web
import app

from config.apilogging import log
from app.service import charges_service
from app.db import charges_operations
from app.model import charge


class ChargeCreation:
    """ This class handles the REST requests to create charges"""

    def POST(self, shop_id):
        """ Method that handles POST calls (creation) """

        log.info("Rest API POST call (creation) - shop_id=%s", shop_id)

        #Gets the POST data
        formData = web.data()

        response_json = None

        #Transforms POST data into a JSON object
        jsonData = json.loads(formData)

        #Creates the charge
        response_json = charges_service.create_charge(shop_id, jsonData)


        return response_json


class ChargeHandle:
    """ This class handles all the REST requests to manage existing charges"""

    def GET(self, charge_id):
        """ Method that handles GET calls (queries) """

        log.info("Rest API GET call (query). Charge ID: " + charge_id)

        #TODO: this is just an example. Replace with real response json
        response_json = charges_service.get_payment_info(charge_id)

        return response_json


    def PUT(self, charge_id):
        """ Method that handles PUT calls (updates) """

        log.info("Rest API PUT call (update). Charge ID: " + charge_id)

        #TODO: this is just an example. Replace with real response json
        charge = app.model.charge.get_charge_obj()
        json_charge = json.dumps(charge)

        return json_charge


    def DELETE(self, charge_id):
        """ Method that handles DELETE calls  """
        log.info("Rest API DELETE call. Charge ID: " + charge_id)

        success = charges_service.delete_charge(int(charge_id))

        #TODO: this is just an example. Replace with real response json
        if (success):
            charge = app.model.charge.get_charge_obj()
            charge["status"] = "deleted"
            json_charge = json.dumps(charge)

            return json_charge

        #Error
        return None

class ChargeRedirect:
    def GET(self, charge_id):
        cache_id = charges_operations.get_cache_id(charge_id)
        log.info("Found cache_id: " + cache_id + " for charge_id: " + charge_id)

        page_to_redirect = charges_service.get_payment_page(cache_id)
        log.info("Redirecting to " + page_to_redirect)
        web.redirect(page_to_redirect)