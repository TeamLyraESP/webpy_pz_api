import db_operations
import db_config

from config.apilogging import log

table = "charges"
db = db_operations.DBGeneric(table, db_config.DB_PATH)


def get_cache_id(charge_id):
    cache_id = db.get_value(field_name="cache_id", where_clause="WHERE charge_id=" + str(charge_id))

    log.info("Retrieved cache_id with value: " + cache_id)
    return cache_id


def get_charge_id(cache_id):
    charge_id = db.get_value(field_name="charge_id", where_clause="WHERE cache_id='" + cache_id + "'")

    log.info("Retrieved charge_id with value: %d" % (charge_id))
    return charge_id


def add_charge(charge):
    query_clause = """INSERT into %s (status, created_at, updated_at, cache_id, trans_id, site_id, mode)
                      VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')""" \
                   % (
    table, charge["status"], charge["created_at"], charge["updated_at"], charge["cache_id"], charge['trans_id'],
    charge['site_id'], charge['mode'])

    try:
        db.execute_query(query_clause)
    except Exception as e:
        log.error("Error adding charge " + e.message)
        return False

    log.info("Added charge to database ")
    return True


def update_charge(charge):
    update_clause = "UPDATE %s set status='%s', updated_at='%s' WHERE charge_id=%d" % (table, charge["status"], charge["updated_at"], int(charge['id']))

    try:
        db.execute_query(update_clause)
    except Exception as e:
        log.error("Error updating charge " + e.message)
        return False

    log.info("Updated charge to database ")
    return True


def delete_charge(charge_id):
    try:
        db.delete_data("WHERE charge_id = %d" % (charge_id))
    except Exception as e:
        log.error("Error deleting charge " + e.message)
        return False

    log.info("Deleted charge from database with id " + str(charge_id))
    return True

def get_charge(charge_id):
    try:
        charge = db.get_data("where charge_id=" + charge_id)
    except Exception as e:
        log.error("Error getting charge " + e.message)
        return None

    log.info("Retrieved charge with id " + charge_id)
    return charge