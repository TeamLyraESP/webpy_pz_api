import sqlite3
import sys


class DBGeneric:
    def __init__(self, table_name, db_path):
        self.table_name = table_name
        if len(db_path) > 0:
            self.db_path = db_path

    def execute_query(self, query_string):
        """Execute a generic query provided by user"""

        conn = self.get_connection(self.db_path)
        try:
            conn.execute(query_string)

            conn.commit()

        finally:
            conn.close()

    def delete_data(self, where_clause):
        """deletes data from the database for the where clause provided"""

        conn = self.get_connection(self.db_path)
        try:
            conn.execute(("DELETE FROM %s " + where_clause) % (self.table_name))

            conn.commit()
        finally:
            conn.close()

    def get_data(self, where_clause):
        """gets the data from the database for the where clause provided"""

        conn = self.get_connection(self.db_path)
        cur = conn.cursor()
        try:
            cur.execute(("SELECT * FROM %s " + where_clause) % (self.table_name))

            table_data = cur.fetchall()

            return table_data
        finally:
            cur.close()
            conn.close()

    def get_value(self, field_name, where_clause):
        """gets one field value from database for the where clause provided"""
        conn = self.get_connection(self.db_path)
        cur = conn.cursor()

        try:
            cur.execute(("SELECT %s FROM %s " + where_clause) % (field_name, self.table_name))

            field_value = cur.fetchone()[0]

            return field_value
        finally:
            cur.close()
            conn.close()


    def get_connection(self, db_path):
        """ initializes the database file"""
        try:
            connection = sqlite3.connect(str(db_path))
            print "Opened database connection"
        except sqlite3.Error, errmsg:
            print 'DB error: ' + str(errmsg)
            sys.exit()

        return connection
