import sqlite3
import db_config

from config.apilogging import log

def init():
    log.info("Initialising data...")
    con = sqlite3.connect(db_config.DB_PATH)

    try:
        with con:

            cur = con.cursor()
            cur.execute("DROP TABLE charges")
            cur.execute("""CREATE TABLE charges(
                            charge_id INTEGER PRIMARY KEY AUTOINCREMENT,
                            status TEXT NOT NULL,
                            created_at DATE NOT NULL,
                            updated_at DATE NOT NULL,
                            cache_id TEXT NOT NULL,
                            trans_id TEXT NOT NULL,
                            site_id TEXT NOT NULL,
                            mode TEXT NOT NULL)""")
            cur.execute("INSERT INTO charges (status, created_at, updated_at, cache_id, trans_id, site_id, mode)  VALUES('incomplete',datetime(), datetime(), '123151891818965', '000096', '91335531', 'test')")

            log.info("Data initialized")
    finally:
        cur.close()
        con.close()

if __name__ == "__main__":
    init()
