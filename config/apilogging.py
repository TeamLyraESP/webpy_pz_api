import logging


class _Log:
    logger = None

    def __init__(self):
        self.logger = logging.getLogger("pz_api_app")
        self.logger.setLevel(logging.DEBUG)

        ch = logging.StreamHandler()

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        ch.setFormatter(formatter)

        self.logger.addHandler(ch)

log = _Log().logger