from time import gmtime, strftime
from random import randint
from config import apiconfig

trans_id_no_duplicates = set()


def resolve_currency_code(currency):
    return apiconfig.currencies[currency]


def resolve_amount_in_cents(amount):
    return int(amount * 100)


def resolve_url_payment():
    return "https://payzen.leon.lyra-network.com/vads-payment/"


def resolve_transaction_time():
    return strftime("%Y%m%d%H%M%S", gmtime())


def resolve_transaction_id():
    #If the set of ids is not initalized we created some into a set making sure they are not duplicated
    if (len(trans_id_no_duplicates) == 0):
        while len(trans_id_no_duplicates) < 10000:
            trans_id_no_duplicates.add(str(randint(0, 99999)).rjust(6, '0'))

    return trans_id_no_duplicates.pop()