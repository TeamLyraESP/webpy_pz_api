import hashlib
import pzutil
import urllib
import urllib2
import re
import json

from config import apiconfig
from config.apilogging import log

from suds.client import Client

WSDLUrl = "https://payzen.leon.lyra-network.com/vads-ws/v4?wsdl"

def create_lyra_payment(shop_id, currency, amount):
    #TODO: create logic to fill in Lyra's V2 form. Current logic is just a draft

    #Data to fill
    v2_form_data = {
        "vads_version": "V2",
        "vads_site_id": shop_id,
        "vads_trans_id": pzutil.resolve_transaction_id(),
        "vads_trans_date": pzutil.resolve_transaction_time(),
        "vads_payment_config": "SINGLE",
        "vads_amount": pzutil.resolve_amount_in_cents(amount),
        "vads_currency": pzutil.resolve_currency_code(currency),
        "vads_ctx_mode": "TEST",
        "vads_page_action": "PAYMENT",
        "vads_action_mode": "INTERACTIVE",
        "vads_url_return": "https://payzen.leon.lyra-network.com/vads-test/order.return.a",
        "vads_return_mode": "GET"
    }

    signature = ""

    for key in sorted(v2_form_data.keys()):
        signature += str(v2_form_data[key]) + "+"
    signature += apiconfig.shop_key

    hashed_signature = hashlib.sha1(signature)
    log.info("Calculated hash: " + hashed_signature.hexdigest())
    v2_form_data["signature"] = str(hashed_signature.hexdigest())

    #Sends form to create order
    data_to_send = urllib.urlencode(v2_form_data)
    req = urllib2.Request(pzutil.resolve_url_payment(), data_to_send)
    response = urllib2.urlopen(req)

    page_response = response.read()

    log.info(page_response)

    reg_cache_id = re.compile('<input type="hidden" name="cacheId" value="([^"]*)" />')
    cache_id = reg_cache_id.search(page_response).group(1)

    log.info("CacheId found: " + cache_id)

    return cache_id


def get_payment_creation_info(cache_id):
    req = urllib2.Request(pzutil.resolve_url_payment() + "service/order?cacheId=" + cache_id)
    response = urllib2.urlopen(req)

    str_response = response.read()

    log.info("Payment info: " + str_response)

    return str_response

def get_payment_page(cache_id):
    return pzutil.resolve_url_payment() + "/exec.refresh.a?cacheId=" + cache_id

def get_payment_info(charge):
    signature = charge[6] + '+' + charge[5] + '+' + str(1) + '+' + "TEST" + '+' + apiconfig.shop_key
    print(signature)
    signature = hashlib.sha1(signature).hexdigest()
    print(signature)
    charge_date = charge[2]
    ws_format_trans_date = charge_date[0:4] + '-' + charge_date[5:7] + '-' + charge_date[8:10] + 'T' + charge_date[11:13] + ':' + charge_date[14:16] + ':' + charge_date[17:19] + '+00:00'
    client = Client(WSDLUrl)
    payment_info = client.service.getInfo(charge[6], ws_format_trans_date, charge[5], 1, "TEST", signature)
    print payment_info

    return payment_info