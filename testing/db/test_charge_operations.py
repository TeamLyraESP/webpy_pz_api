from app.db import init_data
from app.db import charges_operations


if __name__ == "__main__":
    init_data.init()
    charge = {"charge_id" : 5,
              "status" : "incomplete",
              "created_at" : "1970-01-01",
              "updated_at" : "1970-01-01",
              "cache_id" : "151891981891"}
    charges_operations.add_charge(charge)

    print (charges_operations.get_cache_id(5))

    charges_operations.delete_charge(5)
